/*
 * Piero Bozzolo - Javascript Core Extensions
 * Relased under the GNU/GPL
 * 
 * https://bitbucket.org/petecocoon/cocoonjs/
 */

CocoonJS = {};

/*
 * Array core extensions
 */
Array.prototype.unique = function () {
    var arrVal = this;
    var uniqueArr = [];
    for (var i = arrVal.length; i--; ) {
        var val = arrVal[i];
        if (uniqueArr.include(val) === -1) {
            uniqueArr.unshift(val);
        }
    }
    return uniqueArr;
}

Array.prototype.first = function() {return this[0];};

Array.prototype.last = function() {return this[this.length - 1];};

Array.prototype.include = function(element, fromIndex){    
    var len;    
    len=this.length;
    fromIndex = fromIndex ? fromIndex < 0 ? Math.max(0, len + fromIndex) : fromIndex : 0;
    for(;fromIndex < len; fromIndex++){
        if(fromIndex in this && this[fromIndex] === element) return fromIndex
    }   
    return-1    
}

/*
 * String core extensions
 */

String.format = function() {
    var s = arguments[0];
    for (var i = 0; i < arguments.length - 1; i++) {       
        var reg = new RegExp("\\{" + i + "\\}", "gm");             
        s = s.replace(reg, arguments[i + 1]);
    }
    return s;
}

String.prototype.format = function(){
    var args = [this];
    for(var i=0, ii = Object.keys(arguments).length; i<ii; i++)  args.push(arguments[i]);
    return String.format.apply(null, args);
}

String.prototype.endsWith = function (suffix) {
    return (this.substr(this.length - suffix.length) === suffix);
}

String.prototype.startsWith = function(prefix) {
    return (this.substr(0, prefix.length) === prefix);
}

String.prototype.isEmpty = function(){
    return ((this.replace(/^\s+|\s+$/, '').length) == 0);
};

String.prototype.trim = function(){
    return this.replace(/^\s+|\s+$/g,"");
}

String.prototype.camelize = function(firstLetter){    
    var camelizedString = this.replace(/(\_[a-z])/g, function($1){return $1.toUpperCase().replace('_','');}).replace(/\/(.?)/, function($1){$1.toUpperCase()});    
    if(firstLetter){        
       return camelizedString[0].toUpperCase().concat(camelizedString.substr(1));
    }
    return camelizedString;
}

String.prototype.dasherize = function(){
    return this.replace(/([A-Z])/g, function($1){return "-"+$1.toLowerCase();});
}

String.prototype.underscorize = function(){
    return this.replace(/([A-Z])/g, function($1){return "_"+$1.toLowerCase();});
}

String.prototype.multiplies = function(times){
    if(!times) return null;
    var multipliedString = '';
    for (i = 0; i < times; i++) {
        multipliedString += this;
    }
    return multipliedString;
}

String.prototype.swapcase = function(){
    return this.replace(/([a-z]+)|([A-Z]+)/g,function($0,$1,$2){
        return ($1) ? $0.toUpperCase() : $0.toLowerCase();
    })
}

/*
 * Object core extensions
 */
if (!Object.keys) {
    Object.keys = (function () {
        var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({
            toString: null
        }).propertyIsEnumerable('toString'),
        dontEnums = [
        'toString',
        'toLocaleString',
        'valueOf',
        'hasOwnProperty',
        'isPrototypeOf',
        'propertyIsEnumerable',
        'constructor',
        'size'
        ],
        dontEnumsLength = dontEnums.length

        return function (obj) {
            if (typeof obj !== 'object' && typeof obj !== 'function' || obj === null) throw new TypeError('Object.keys called on non-object')

            var result = []

            for (var prop in obj) {
                if (hasOwnProperty.call(obj, prop)) result.push(prop)
            }

            if (hasDontEnumBug) {
                for (var i=0; i < dontEnumsLength; i++) {
                    if (hasOwnProperty.call(obj, dontEnums[i])) result.push(dontEnums[i])
                }
            }
            return result
        }
    })()
}

Object.prototype.size = function(){
    return this.keys().length;
}